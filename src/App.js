import React from 'react'
import { BrowserRouter ,Routes,Route, Navigate} from 'react-router-dom'
import NavBar from './components/navBar/NavBar'
import User from './components/users/User'
import Register from './components/auth/Register'
import Login from './components/auth/Login'
import { useUserContext } from './services/Provider'
import Progress from './components/animation/Progress'
import Dashboard from './components/dashboard/Dashboard'
import PageChoix from './components/admin/PageChoix'
import "./App.css"
import MessageSuccess from './components/auth/MessageSuccess'
import ForgetPassword from './components/auth/ForgetPassword'
import ResetPassword from './components/auth/ResetPassword'
export default function App() {

  const {isLoading}=useUserContext()
  if(isLoading){
       return    <div className='loading container-fluid'> <Progress/>   </div>
  }

 
  return (
    <BrowserRouter>
            <Routes>
                  <Route path='/' element={<NavBar/>}></Route>
                  <Route path='/register' element={<Register/>}></Route>
                   <Route path='/login' element={<Login/>}></Route>
                  <Route path='/users' element={<User/>}></Route>
                  <Route path='/dashboard' element={<Dashboard/>}></Route>
                  <Route path='/pageChoix' element={<PageChoix/>}></Route>
                  <Route path='/forgot-password' element={<ForgetPassword/>}></Route>
                  <Route path='/messsage-success' element={<MessageSuccess/>}></Route>
                  <Route path='/update-password/:token' element={<ResetPassword/>}></Route>
                  
                  
                  
            </Routes>
    </BrowserRouter>
  )
}
