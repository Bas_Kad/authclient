import React, { useState,useEffect, useContext, createContext } from 'react';
import { useQuery } from 'react-query';
import { getCookies } from './auth/auth';
import {jwtDecode} from 'jwt-decode';
// Corrected typo: UserContext
const UserContext = createContext();

function useUserContext() {
    return useContext(UserContext);
}

export default function Provider({ children }) {
    const [userAuth, setUserAuth] = useState({
        isAuthenticated: false ,
        role:null
    });

    const { data: token, isLoading, isError } = useQuery(['token'], async () => {
        try {
            const response = await getCookies();
            return response.data;
        } catch (error) {
            console.error('Une erreur est survenue ', error);
            throw error;
        }
    });

  
    useEffect(() => {
        if (token) {
            const tokendecoded=jwtDecode(token)
            const result=tokendecoded.roles.join(',');
            const roleResult=result==="admin"?1:0;
            setUserAuth(prevState => ({ ...prevState,role:roleResult, isAuthenticated: true }));
        }
    }, [token]);

    return (
        <UserContext.Provider value={{ userAuth, isLoading }}>
            {children}
        </UserContext.Provider>
    );
}

export { useUserContext };
