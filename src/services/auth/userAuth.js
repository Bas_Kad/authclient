import api from '../api'
async function getUsers(){
     try{
         const response=await api.get('/users')
         return response
     }
     catch(error){
        throw error
     }
}
export {getUsers}