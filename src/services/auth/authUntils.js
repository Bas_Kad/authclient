function getEmail(){
      return localStorage.getItem('user_email')
}

function setEmail(email){
      localStorage.setItem('user_email',email)
}

export {getEmail,setEmail}