import api from '../api'
async function register(data){
     try{
         const response=await api.post('/register',data)
         return response
     }
     catch(error){
        throw error
     }
}

async function login(data){
    try{
        const response=await api.post('/login',data)
        return response
    }
    catch(error){
       throw error
    }
}

async function logout(){
    try{
        const response=await api.get('/logout')
        return response
    }
    catch(error){
       throw error
    }
}


async function forgotPassword(data){
    try{
        const response=await api.post('/forgot-password',data)
        return response
    }
    catch(error){
       throw error
    }
}

async function resetPassword(data){
    try{
        const response=await api.post('/reset-password',data)
        return response
    }
    catch(error){
       throw error
    }
}

async function getCookies(){
    try{
        const response=await api.get('cookies')
        return response
    }
    catch(error){
       throw error
    }
}
export {register,resetPassword,forgotPassword,getCookies,logout,login}