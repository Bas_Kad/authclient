import axios from "axios";

const api = axios.create({
  baseURL: "http://localhost:3001",
  headers: {
    "Content-Type": "application/json" // Correction de la faute de frappe ici
  },
  withCredentials: true
});

export default api;
