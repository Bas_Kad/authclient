import { useUserContext } from "./Provider";

function useAuth() {
  const { userAuth } = useUserContext();
  return userAuth;
}

export { useAuth };
