import React from 'react';
import { Link, Navigate } from 'react-router-dom';
import { useAuth } from '../../services/useAuth';
import './pageChoix.css';
import Alert from '@mui/material/Alert';
import CheckIcon from '@mui/icons-material/Check';

export default function PageChoix() {
  const { role, isAuthenticated } = useAuth();

  if (role === 0) {
    return <Navigate to={"/"} />;
  }

  if (!isAuthenticated) {
    return <Navigate to={"/login"} />;
  }

  return (
    <div className='page-choix container-fluid'>
      <h4 className='h4 text-primary'>Vous êtes administrateur</h4>
      <p>Quelle page souhaitez-vous consulter ?</p>
      <div className={'choixpages'}>
        <Link className='liens pages liens1' to={"/"}>
          Utilisateur
        </Link>
        <Link className='liens pages liens1' to={'/dashboard'}>
          Administrateur
        </Link>
      </div>
    </div>
  );
}
