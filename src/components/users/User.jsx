import React from 'react'
import { getUsers } from '../../services/auth/userAuth'
import { useQuery } from 'react-query'
import Progress from '../animation/Progress'
import { useAuth } from '../../services/useAuth'
import { Navigate } from 'react-router-dom'
export default function User() {
  const {isAuthenticated}=useAuth()
  const {data:users,isLoading,isError}=useQuery(['users_list'],async ()=>{
        try{
           const response=await getUsers()
           return response.data
        }
        catch(error){
            console.error('Une erreur est survenue ',error)
        }
  })

  if(isLoading){
       return(
        
        <Progress />
      
       )
  }

  if(!isAuthenticated){
         return  <Navigate to={"/login"} />
  }
  if(!users){
       return <div>
             Aucun user !
       </div>
  }
  return (
    <div>
          <ul>
                {
                  users.map((element,index)=>{
                      return  <li key={index}>
                            <h3>{element.id}</h3>
                            <p>{element.email}</p>
                      </li>
                  })
                }
          </ul>
    </div>
  )
}
