import React from 'react';
import './description.css';
import './nav.css'
export default function Description() {
  return (
    <div className='description'>
        <h1>Emploi du <span className='text-primary'>Temps</span> </h1>
        <p className='p1'>Bienvenue sur notre plateforme de consultation d'emplois du temps. Accédez à votre emploi du temps en un instant, où que vous soyez.</p>

        <form action="">
            <input className='inputMessage' type="text" placeholder="Entrez votre recherche..." />
            <button className="liens buttonD liens1">Rechercher</button>
        </form>
        <p className='p2'>Cliquez sur le bouton pour découvrir votre emploi du temps personnalisé.</p>
    </div>
  );
}