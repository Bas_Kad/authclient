import React from 'react';
import { Link, Outlet } from 'react-router-dom';
import { useAuth } from '../../services/useAuth';
import { useMutation, useQueryClient } from 'react-query';
import { logout } from '../../services/auth/auth';
import './nav.css'; // Assurez-vous que ce chemin est correct
import Description from './Description';
import { BsBrowserEdge } from "react-icons/bs";
import { MdDarkMode } from "react-icons/md";

export default function NavBar() {
  const { isAuthenticated } = useAuth();

  const queryClient = useQueryClient();
  const { mutate, isLoading } = useMutation(
    async () => {
      try {
        const response = await logout();
        return response.data;
      } catch (error) {
        console.error('Erreur lors de la déconnexion!', error);
        throw error;
      }
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries('token');
        window.location.href = "/";
      },
    }
  );

  function handleLogout() {
    mutate();
  }

  return (
    <section className="container-fluid containernav"> {/* Ajout de guillemets autour de la chaîne de classe */}
      <ul className="ulnav"> {/* Correction de la syntaxe de classe */}
        <li style={{fontSize:"20px"}} className='titrenavspan'> {/* Utilisation de li au lieu de span pour les éléments de la barre de navigation */}
          <BsBrowserEdge className='text-primary'  style={{fontSize:"20px", marginRight:"3px"}} />
          <span>E-<span className='text-primary'>Emploi</span> </span>
        </li>
        <li className='navs'> {/* Utilisation de li au lieu de span pour les éléments de la barre de navigation */}
          <li>
                <span className='darkmode'> <MdDarkMode className='dark'/></span>
          </li>
          {!isAuthenticated && (
            <>
              <Link className='liens liens2' to="/login">Se connecter</Link>
              <Link className='liens liens1' to="/register">S'inscrire</Link>
            </>
          )}
          {isAuthenticated && (
            <>
              <Link  className='liens liens2'   to="/users">Profil</Link>
              <button  className='bg-danger liens ' onClick={handleLogout}>
                {isLoading ? 'Déconnexion en cours...' : 'Déconnexion'}
              </button>
            </>
          )}
          
        </li>
      </ul>
      <Outlet />
      <Description />
    </section>
  );
}
