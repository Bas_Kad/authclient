import React from 'react'
import { Navigate } from 'react-router-dom'
import { useAuth } from '../../services/useAuth'
export default function Dashboard() {
  const {role ,isAuthenticated}=useAuth()

  if(!isAuthenticated){
      return <Navigate to={"/login"}/>
  }

  return (
    <div>
           <h3>Vous ettes sur le dashboard</h3>
    </div>
  )
}
