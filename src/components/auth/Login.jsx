import React, { useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { login } from '../../services/auth/auth';
import { Navigate, Link, useNavigate } from 'react-router-dom';
import { useAuth } from '../../services/useAuth';
import AnimComponent from '../animation/AnimComponent';
import { BsBrowserEdge } from "react-icons/bs";

import { getEmail,setEmail } from '../../services/auth/authUntils';
const regexEmail = /^[\w\.-]+@[a-zA-Z\d\.-]+\.[a-zA-Z]{2,}$/;

export default function Login() {
  const navigate = useNavigate()
  const { isAuthenticated, role } = useAuth();
  const queryClient = useQueryClient()
  const [data, setData] = useState({
    email: getEmail()?getEmail():"",
    password: "",
    remember: false
  });

  const [errorMessages, setErrorMessages] = useState({
    emailError: "",
    passwordError: "",
    errorServer: {}
  });

  const mutation = useMutation(async (data) => {
    try {
      const response = await login(data);
      setEmail(data.email)
      return response.data;
    } catch (error) {
      console.error('Erreur lors de la connexion!', error);
      setErrorMessages(prev => ({ ...prev, errorServer: error.response.data }));
      throw error;
    }
  }, {
    onSuccess: () => {
      queryClient.invalidateQueries('token');
      navigate('/pageChoix');
    }
  });

  const validateEmail = () => {
    if (!data.email.trim()) {
      setErrorMessages(prev => ({ ...prev, emailError: "Veuillez fournir une adresse e-mail." }));
      return false;
    } else if (!regexEmail.test(data.email)) {
      setErrorMessages(prev => ({ ...prev, emailError: "Adresse e-mail invalide." }));
      return false;
    }
    setErrorMessages(prev => ({ ...prev, emailError: "" }));
    return true;
  }

  const validatePassword = () => {
    if (!data.password.trim()) {
      setErrorMessages(prev => ({ ...prev, passwordError: "Veuillez fournir un mot de passe." }));
      return false;
    }
    setErrorMessages(prev => ({ ...prev, passwordError: "" }));
    return true;
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    const isEmailValid = validateEmail();
    const isPasswordValid = validatePassword();

    if (isEmailValid && isPasswordValid) {
      mutation.mutate(data);
    }
  }

  const handleChange = (e) => {
    const { name, value, checked } = e.target;
    const newValue = name === "remember" ? checked : value;
    setData(prevData => ({ ...prevData, [name]: newValue }));
  };

  if (isAuthenticated) {
    return role === 1 ? <Navigate to="/pageChoix" /> : <Navigate to="/" />;
  }

  return (
    <section className='section container-fluid'>
      <article className='sous-section' style={{padding:"50px 30px"}}>
        <span className='inconsAuth'>
            <BsBrowserEdge></BsBrowserEdge>
        </span>
        <form onSubmit={handleSubmit} className='enfant2'>
          <div className="groupe g1">
            <label className='label' htmlFor="email">
              <span className="text">Email</span>
              {errorMessages.errorServer && errorMessages.errorServer.errorEmail ? (
                <span className="errorText">{errorMessages.errorServer.errorEmail}</span>
              ) : errorMessages.emailError && <span className="errorText">{errorMessages.emailError}</span>}
            </label>
            <input
              className={`input ${errorMessages.emailError || (errorMessages.errorServer && errorMessages.errorServer.errorEmail) ? 'errorMessage' : ''}`}
              type="text"
              id="email"
              placeholder='email@exemple.com'
              name="email"
              value={data.email}
              onChange={handleChange}
            />
          </div>
          <div className="groupe g2">
            <label className='label' htmlFor="password">
              <span className='text'>Mot de passe</span>
              <Link to="/forgot-password" className='lien'>Mot de passe oublié ?</Link>
            </label>
            <input
              className={`input ${errorMessages.passwordError || (errorMessages.errorServer && errorMessages.errorServer.errorPassword) ? 'errorMessage' : ''}`}
              type="password"
              id="password"
              placeholder='*********'
              name="password"
              value={data.password}
              onChange={handleChange}
              
            />
            {errorMessages.errorServer && errorMessages.errorServer.errorPassword ? (
              <span className="errorText">{errorMessages.errorServer.errorPassword}</span>
            ) : errorMessages.passwordError && <span className="errorText">{errorMessages.passwordError}</span>}
          </div>
          <div className="groupe g3">
            <button className='liens button liens1'
              style={{ backgroundColor: mutation.isLoading ? "rgba(29, 120, 120, 0.595)" : "" }}
              disabled={mutation.isLoading}>
              {mutation.isLoading ? (
                <span><AnimComponent borderColor={'white'} bord={2} padChild={4} padParent={11} /></span>
              ) : "Se connecter"}
            </button>
            {errorMessages.errorServer && errorMessages.errorServer.database && <span className="errorText">{errorMessages.errorServer.database}</span>}
          </div>
          <div className='groupe'>
            <span className='labels'>
              Vous n'avez pas de compte ?
              <Link className='lien' to={'/register'}> S'inscrire</Link>
            </span>
          </div>
        </form>
      </article>
    </section>
  );
}
