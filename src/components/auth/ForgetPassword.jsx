import React, { useState } from 'react';
import { Link,useNavigate } from 'react-router-dom'; // Assurez-vous d'importer Link depuis react-router-dom
import AnimComponent from '../animation/AnimComponent';
import { BsBrowserEdge } from "react-icons/bs";
import {  useMutation } from 'react-query';
import { getEmail } from '../../services/auth/authUntils';
import { forgotPassword } from '../../services/auth/auth';
const regexEmail = /^[\w\.-]+@[a-zA-Z\d\.-]+\.[a-zA-Z]{2,}$/;

export default function ForgetPassword() {
  const navigate=useNavigate()
  const [data, setData] = useState({ email:  getEmail()?getEmail():"" });
  const [errorMessages, setErrorMessages] = useState({ emailError: '', errorServer: { errorEmail: '', database: '' } });
     
  const {mutate,isLoading,isError} = useMutation(async (data) => {
    try {
      const response = await forgotPassword(data);
      console.log(response)
      return response.data;
    } catch (error) {
      console.error('Erreur lors de la connexion!', error);
      setErrorMessages(prev => ({ ...prev, errorServer: error.response.data }));
      throw error;
    }
  }, {
    onSuccess: () => {
      navigate('/messsage-success');
    }
  });
  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const validateEmail = () => {
    if (!data.email.trim()) {
        setErrorMessages(prev => ({ ...prev, emailError: "Veuillez fournir une adresse e-mail." }));
        return false;
    } else if (!regexEmail.test(data.email)) {
        setErrorMessages(prev => ({ ...prev, emailError: "Adresse e-mail invalide." }));
        return false;
    }
    setErrorMessages(prev => ({ ...prev, emailError: "" }));
    return true;
}

  const handleSubmit = (e)=> {
    e.preventDefault();
    if(!validateEmail()){
           return 
    }
      mutate(data)
  }
  return (
    <section className='section container-fluid'>
      <article className='sous-section-oublier' >
      <span className='inconsAuth'>
            <BsBrowserEdge></BsBrowserEdge>
        </span>
        <form onSubmit={handleSubmit} className='enfant2'>
          <div className="groupe g1">
            <label className='label' htmlFor="email">
              <span className="text">Email</span>
              {errorMessages.errorServer.errorEmail && <span style={{ color: "red" }}>{errorMessages.errorServer.errorEmail}</span>}
              {errorMessages.emailError && <span style={{ color: "red" }}>{errorMessages.emailError}</span>}
            </label>
            <input
              className={`input`}
              type="text"
              id="email"
              style={{border: errorMessages.emailError || errorMessages.errorServer.errorEmail ? "1px solid red" : ""}}
              placeholder='email@exemple.com'
              name="email"
              value={data.email}
              onChange={handleChange}
            />
          </div>
          <div className="groupe g3">
            <button className='liens button liens1' >
              {isLoading ? (
               <span><AnimComponent borderColor={'white'} bord={2} padChild={4} padParent={11} /></span>
              ) : "Envoyer l'email pour restaurer le mot de passe"}
            </button>
            {errorMessages.errorServer.database && (
              <span style={{ color: "red", fontSize: "14px", marginTop: "6px" }}>{errorMessages.errorServer.database}</span>
            )}
          </div>
          <div style={{ marginTop: "-4px" }} className='groupe'>
            <span className='labels'>
              Rappelez-vous de mot de passe ?
              <Link className='lien' to={'/login'}> Se connecter</Link>
            </span>
          </div>
        </form>
      </article>
    </section>
  );
}
