import React from 'react';
import { Link } from 'react-router-dom';
import { AiOutlineDownCircle } from 'react-icons/ai';

export default function ResetPasswordMessage() {
    return (
        <section className='section container-fluid'>
            <article className='sous-section-message'>
                <span className='reset-password-icon'>
                    <AiOutlineDownCircle className='inconsAuth' style={{ fontSize: '70px' }} />
                </span>
                <div>
                    <p>Un email de réinitialisation de mot de passe a été envoyé à votre adresse. Veuillez vérifier votre boîte de réception.</p>
                </div>
                <div style={{paddingBottom:"10px"}}>
                    <Link to={"/"} className='reset-password-link'>Retour à la page d'accueil</Link>
                </div>
            </article>
        </section>
    );
}
