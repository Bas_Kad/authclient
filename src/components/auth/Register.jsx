import React, { useState } from 'react';
import {useQueryClient, useMutation } from 'react-query';
import { register } from '../../services/auth/auth';
import { useAuth } from '../../services/useAuth';
import {  useNavigate, Link, Navigate } from 'react-router-dom';
import AnimComponent from '../animation/AnimComponent';
import './auth.css';
import { BsBrowserEdge } from "react-icons/bs";

const regexEmail = /^[\w\.-]+@[a-zA-Z\d\.-]+\.[a-zA-Z]{2,}$/;

export default function Register() {
  const { isAuthenticated, role } = useAuth();
  const navigate = useNavigate();

  const [data, setData] = useState({
    nom: "",
    email: "",
    password: "",
    confirmation: "",
    remember: false
  });

  const [errorMessages, setErrorMessages] = useState({
    nomError: "",
    emailError: "",
    passwordError: "",
    confirmationError: "",
    errorServer: {}
  });

  const queryClient = useQueryClient();

  const mutation = useMutation(async (data) => {
    try {
      const response = await register(data);
      console.log(response);
      return response.data;
    } catch (error) {
      console.error('Erreur lors de l\'enregistrement!', error);
      setErrorMessages(prev=>({...prev,errorServer:error.response.data}))
      throw error;
    }
  }, {
    onSuccess: () => {
      queryClient.invalidateQueries('token');
      navigate('/pageChoix');
    }
  });

  const validateNom = () => {
    if (!data.nom.trim()) {
      setErrorMessages(prev => ({ ...prev, nomError: "Veuillez fournir un nom." }));
      return false;
    }
    setErrorMessages(prev => ({ ...prev, nomError: "" }));
    return true;
  }

  const validateEmail = () => {
    if (!data.email.trim()) {
      setErrorMessages(prev => ({ ...prev, emailError: "Veuillez fournir une adresse e-mail." }));
      return false;
    } else if (!regexEmail.test(data.email)) {
      setErrorMessages(prev => ({ ...prev, emailError: "Adresse e-mail invalide." }));
      return false;
    }
    setErrorMessages(prev => ({ ...prev, emailError: "" }));
    return true;
  }

  const validatePassword = () => {
    if (!data.password.trim()) {
      setErrorMessages(prev => ({ ...prev, passwordError: "Veuillez fournir un mot de passe." }));
      return false;
    }
    setErrorMessages(prev => ({ ...prev, passwordError: "" }));
    return true;
  }

  const validateConfirmation = () => {
    if (data.password !== data.confirmation) {
      setErrorMessages(prev => ({ ...prev, confirmationError: "Les mots de passe ne correspondent pas." }));
      return false;
    }
    setErrorMessages(prev => ({ ...prev, confirmationError: "" }));
    return true;
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    const isNomValid = validateNom();
    const isEmailValid = validateEmail();
    const isPasswordValid = validatePassword();
    const isConfirmationValid = validateConfirmation();

    if (isNomValid && isEmailValid && isPasswordValid && isConfirmationValid) {
      mutation.mutate(data);
    }
  }

  const handleChange = (e) => {
    const { name, value, checked } = e.target;
    const newValue = name === "remember" ? checked : value;
    setData(prevData => ({ ...prevData, [name]: newValue }));
  };

  if (isAuthenticated) {
    return role === 1 ? <Navigate to="/pageChoix" /> : <Navigate to="/" />;
  }

  return (
    <section className='section container-fluid'>
      <article className='sous-section'>
      <span className='inconsAuth'>
            <BsBrowserEdge></BsBrowserEdge>
        </span>
        <form onSubmit={handleSubmit} className='enfant2'>
          <div className="groupe g1">
            <label className='label' htmlFor="nom">
              <span className="text">Nom</span>
              {errorMessages.nomError && <span style={{ color: "red" }}>{errorMessages.nomError}</span>}
            </label>
            <input
              className={`input`}
              type="text"
              id="nom"
              style={{ border: errorMessages.nomError && "1px solid red" }}
              placeholder='Nom'
              name="nom"
              value={data.nom}
              onChange={handleChange}
            />
          </div>

          <div style={{ marginTop: "-3px" }} className="groupe g1">
            <label className='label' htmlFor="email">
              <span className="text">Email</span>
              {errorMessages.emailError && <span style={{ color: "red" }}>{errorMessages.emailError}</span>}
            </label>
            <input
              className={`input`}
              type="text"
              style={{ border: (errorMessages.emailError || errorMessages.errorServer.errorEmail) && "1px solid red" }}
              id="email"
              placeholder='email@exemple.com'
              name="email"
              value={data.email}
              onChange={handleChange}
            />
            {errorMessages.errorServer.errorEmail && <span style={{ color: "red", fontSize: "14px" }}>{errorMessages.errorServer.errorEmail}</span>}
          </div>
          <div className="groupe g2">
            <label className='label' htmlFor="password">
              <span className='text'>Mot de passe</span>
              {errorMessages.passwordError && <span style={{ color: "red", fontSize: "14px" }}>{errorMessages.passwordError}</span>}
            </label>
            <input
              className={`input`}
              type="password"
              id="password"
              placeholder='*********'
              name="password"
              style={{ border: errorMessages.passwordError && "1px solid red" }}
              value={data.password}
              onChange={handleChange}
            />
          
          </div>
          <div style={{ marginBottom: "9px" }} className="groupe g2">
            <label className='label' htmlFor="confirmation">
              <span className='text'>Confirmation</span>
            </label>
            <input
              className={`input`}
              type="password"
              id="confirmation"
              style={{ border: errorMessages.confirmationError && "1px solid red" }}
              placeholder='*********'
              name="confirmation"
              value={data.confirmation}
              onChange={handleChange}
            />
            {errorMessages.confirmationError && <span style={{ color: "red", fontSize: "14px" }}>{errorMessages.confirmationError}</span>}
          </div>
          <div className="groupe g3">
            <button className='liens button liens1'
              style={{ backgroundColor: mutation.isLoading ? "rgba(29, 120, 120, 0.595)" : "" }}
              disabled={mutation.isLoading}>
              {mutation.isLoading ? (
                <span><AnimComponent borderColor={'white'} bord={2} padChild={4} padParent={11} /></span>
              ) : "S'inscrire"}
            </button>
            {errorMessages.errorServer && <span style={{ color: "red", fontSize: "14px", marginTop: "6px" }}>
              {errorMessages.errorServer.database}
            </span>}
          </div>
          <div style={{ marginTop: "-8px" }} className='groupe'>
            <span className='labels'>
              Vous avez déjà un compte ?
              <Link className='lien' to={'/login'}> Se connecter</Link>
            </span>
          </div>
        </form>
      </article>
    </section>
  );
}
